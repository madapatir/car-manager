package edu.ait.carmanager.controllers;

import edu.ait.carmanager.dto.Cars;
import edu.ait.carmanager.exceptions.CarNotFoundException;
import edu.ait.carmanager.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
public class CarController {
	@Autowired
	CarRepository carRepository;

	@GetMapping("cars")
	public List<Cars> getAllCars() {
		return carRepository.findAll();
	}

	@GetMapping("cars/{carId}")
	public Cars getCarById(@PathVariable int carId) {
		Optional<Cars> foundCar = carRepository.findById(carId);
		if (foundCar.isPresent()) {
			return foundCar.get();
		} else {
			throw new CarNotFoundException("Unable to find car with ID: " + carId);
		}
	}

	@DeleteMapping("cars/{carId}")
	public void deleteCarById(@PathVariable int carId) {
		try {
			carRepository.deleteById(carId);
		} catch (EmptyResultDataAccessException e) {
			throw new CarNotFoundException("Unable to delete car with ID: " + carId);
		}
	}

	@PostMapping("cars/")
	public ResponseEntity createCar(@RequestBody Cars newCar) {
		carRepository.save(newCar);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{id}").buildAndExpand(newCar.getId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@PutMapping("cars/")
	public ResponseEntity updateCar(@RequestBody Cars newCar) {

		if (newCar.getId() != null) {
			carRepository.save(newCar);
			// return 200 ok
			return ResponseEntity.status(HttpStatus.OK).build();
		} else {
			//save new car
			Cars saveCar = carRepository.save(newCar);
			//create the location header
			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{id}").buildAndExpand(newCar.getId())
					.toUri();
			return ResponseEntity.created(location).build();
		}
	}

	@GetMapping("carsByYear/{year}")
	public List<Cars> findCarsByYear(@PathVariable("year") int year) {
		List<Cars> foundCar = carRepository.findCarsByYear(year);
		return foundCar;

	}

	@GetMapping("cars/{make}/{model}")
	public List<Cars> findCarsByMakeAndModel(@PathVariable String make, @PathVariable String model) {
		List<Cars> foundCar = carRepository.findCarsByMakeAndModel(make, model);
		return foundCar;
	}

	@RequestMapping(value = "cars/mileagelessthan/{mileage}", method = RequestMethod.GET)
	public List<Cars> findCarsByMileage(@PathVariable int mileage) {
		List<Cars> foundCar = carRepository.findCarsByMileageLessThan(mileage);
		return foundCar;
	}
}
