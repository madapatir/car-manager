package edu.ait.carmanager.repositories;

import edu.ait.carmanager.dto.Cars;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarRepository extends JpaRepository<Cars, Integer> {

    List<Cars> findCarsByYear(int year);
    List<Cars> findCarsByMakeAndModel(String make,String model);
    List<Cars> findCarsByMileageLessThan(int mileage);


}
